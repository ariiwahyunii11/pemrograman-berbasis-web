<?php

//pendeklarasian variabel 
if (isset($_POST['submit'])) {
$namamhs = $_POST['namamhs'];
$mapel   = $_POST['mapel'];
$tugas   = $_POST['tugas'];
$uts     = $_POST['uts'];
$uas     = $_POST['uas'];

//penjumlahan nilai akhir
$nilai_akhir = ($_POST['tugas']*0.15) + ($_POST['uts'] * 0.35) + ($_POST['uas']*0.5);

//pengkondisian grade nilai berdasarkan dari nilai akhir
    if ($nilai_akhir>=90 && $nilai_akhir<=100)
{
    $grade_nilai="A";
}

elseif ($nilai_akhir>70 && $nilai_akhir<90)
{
    $grade_nilai="B";
}

elseif ($nilai_akhir>50 && $nilai_akhir<=70)
{
    $grade_nilai="C";
}

elseif ($nilai_akhir<=50)
{
    $grade_nilai="D";
}
else
{
    $grade_nilai="E";
}
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Hitung Nilai Mahasiswa</title>
  </head>


  <body>
      <div class="container">
//header teks
      <h2 class="alert alert-primary text-center mt-3" >Form Hitung Nilai Mahasiswa</h2> 
//form input data nilai mahasiswa
        <div class="row justify-content-center">
            <div class="col-8 border border-primary mt-3 p-3">
                <form action="" method="POST">
//input nama lengkap
                    <div class="mb-3">
                        <label for="namamhs" class="form-label">Nama Lengkap</label>
                        <input type="text" name="namamhs" class="form-control" id="namamhs">
                    </div>
//input mata pelajaran
                    <div class="mb-3">
                        <label for="mapel" class="form-label">Mata Pelajaran</label>
                        <input type="text" name="mapel" class="form-control" id="mapel">
                    </div>
//input nilai tugas
                    <div class="mb-3">
                        <label for="tugas" class="form-label">Nilai Tugas</label>
                        <input type="number" name="tugas" class="form-control" id="tugas">
                    </div>
//input nilai uts
                    <div class="mb-3">
                        <label for="uts" class="form-label">Nilai UTS</label>
                        <input type="number" name="uts" class="form-control" id="uts">
                    </div>
//input nilai uas
                    <div class="mb-3">
                        <label for="uas" class="form-label">Nilai UAS</label>
                        <input type="number" name="uas" class="form-control" id="uas">
                    </div>
//button submit
                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        
        <?php if(isset($_POST['submit'])) :?>
//tampilan output data nilai mahasiswa
        <div class="row justify-content-center">
            <div class="col-8 border border-primary mt-3 p-3">
                <div class="alert alert-success">
                    Nama Lengkap    :<?php echo $namamhs ?><br>
                    Mata Pelajaran  :<?php echo $mapel ?><br>
                    Nilai Tugas     :<?php echo $tugas ?><br>
                    Nilai UTS       :<?php echo $uts ?><br>
                    Nilai UAS       :<?php echo $uas ?><br><br>
                    Total Nilai     :<?php echo $nilai_akhir ?><br>
                    Grade Nilai     :<?php echo $grade_nilai ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>  
   

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>